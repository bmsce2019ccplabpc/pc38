#include<stdio.h>
void swap(int *a,int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
}
int main()
{
int n1,n2;
printf("Enter first Number");
scanf("%d",&n1);
printf("Enter second number");
scanf("%d",&n2);
printf("Value of n1 and n2 before fn call %d%d",n1,n2);
swap(&n1,&n2);
printf("Value of n1 and n2 after fn call %d%d",n1,n2);
return 0;
}

